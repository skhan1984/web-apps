var canvas = document.getElementById('canvas'),
	context = canvas.getContext('2d'),
	bBox = canvas.getBoundingClientRect(),
	pressed = false,
	img = new Image(),
	link = document.createElement("a");	

context.strokeStyle = 'blue';
img.src = 'img.png';
img.onload = function(){
	context.drawImage(img,0,0,768,1024);	
	canvas.onmousemove = function(e){
		if(pressed){
			var loc = windowToCanvas(e.pageX, e.pageY);			
			context.lineTo(loc.x, loc.y);
			context.stroke();		
		}
		e.preventDefault();
	}
	
	canvas.onmousedown = function(e){
		pressed = true;
		context.beginPath();			
		var loc = windowToCanvas(e.pageX, e.pageY);	
		e.preventDefault();	
	}
	
	canvas.onmouseup = function(e){
		pressed = false;
		e.preventDefault();
	}
}	

function windowToCanvas(x,y){
	return {
		x: (x - bBox.left)/(bBox.right-bBox.left)*canvas.width,
		y: (y - bBox.top)/(bBox.bottom-bBox.top)*canvas.height
	};
}
	
var link = document.createElement("a");	
link.innerHTML = "Click here to download the file";
link.download = "file.png";
document.getElementById("toolbar").appendChild(link);

function handleExport(){
	canvas.toBlob(function(blob){
		console.log(blob);
		var blobURL = URL.createObjectURL(blob);		
		link.href = blobURL;		
	});
}

function handleClear(){
	context.clearRect(0,0,canvas.width,canvas.height);
	context.drawImage(img,0,0,768,1024);
	context.beginPath();
}
