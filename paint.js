var canvas = document.getElementById('canvas'),
	context = canvas.getContext('2d'),
	bBox = canvas.getBoundingClientRect(),
	pressed = false;


function windowToCanvas(x,y){
	return {
		x: x - bBox.left * (canvas.width / bBox.width),
		y: y - bBox.top * (canvas.height / bBox.height)
	};
}

context.beginPath();

canvas.onmousemove = function(e){
	if(pressed){
		var loc = windowToCanvas(e.clientX, e.clientY);
		//console.log(loc);
		context.lineTo(loc.x, loc.y);
		context.stroke();		
	}
}

canvas.onmousedown = function(e){
	pressed = true;
	var loc = windowToCanvas(e.clientX, e.clientY);
	context.moveTo(loc.x,loc.y);
}

canvas.onmouseup = function(e){
	pressed = false;
}
